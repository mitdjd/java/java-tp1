/**
 * App
 */
public class App {

   public static void main(String[] args) {
      Point p = new Point(5.8, 10.2);
      // p.show();
      double hAxis = p.getHAxis();
      double vAxis = p.getVAxis();
      //
      System.out.println(hAxis+" , "+vAxis);
   }
}
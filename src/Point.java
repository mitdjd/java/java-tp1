/**
 * Point
 */
public class Point {

   String name;
   private Double vAxis;
   private Double hAxis;

   public Point(Double vAxis) {
      this.vAxis = vAxis;
   }

   public Point(Double vAxis, Double hAxis) {
      this.vAxis = vAxis;
      this.hAxis = hAxis;
   }

   public double getHAxis() {
      return this.hAxis;
   }

   public double getVAxis() {
      return this.vAxis;
   }
   
}